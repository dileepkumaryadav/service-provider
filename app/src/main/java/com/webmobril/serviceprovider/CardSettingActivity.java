package com.webmobril.serviceprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CardSettingActivity extends AppCompatActivity {
    ImageView imgarrowback;
    TextView tetaddcard,tetedit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardsetting);
        tetaddcard=findViewById(R.id.tetaddcard);
        tetedit=findViewById(R.id.tetedit);
        imgarrowback=findViewById(R.id.arrrowcardsetting);
        imgarrowback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(CardSettingActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        tetaddcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(CardSettingActivity.this,AddcardActivtity.class);
                startActivity(intent);
                finish();
            }
        });
        tetedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(CardSettingActivity.this,EditcardActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}