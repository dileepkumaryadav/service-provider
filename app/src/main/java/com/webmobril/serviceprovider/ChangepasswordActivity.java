package com.webmobril.serviceprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class ChangepasswordActivity extends AppCompatActivity {
    ImageView imgarrowback;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);
        imgarrowback=findViewById(R.id.imgarrrowchangepassword);
        imgarrowback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ChangepasswordActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}