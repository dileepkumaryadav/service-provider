package com.webmobril.serviceprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageView;

public class ContactusActivity extends AppCompatActivity {
    ImageView imgarrowback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);
        imgarrowback=findViewById(R.id.arrrowcotactus);
        imgarrowback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ContactusActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}