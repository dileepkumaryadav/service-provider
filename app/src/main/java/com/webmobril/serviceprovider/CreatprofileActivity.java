package com.webmobril.serviceprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class CreatprofileActivity extends AppCompatActivity {
    ImageView imgback;
    Button btn;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creatprofile);
        imgback=findViewById(R.id.imgbackprofile);
        btn=findViewById(R.id.btncontinue);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(CreatprofileActivity.this,DocumentActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(CreatprofileActivity.this,MainActivity.class);
                startActivity(i);
            }
        });
    }
}