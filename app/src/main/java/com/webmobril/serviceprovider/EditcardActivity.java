package com.webmobril.serviceprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class EditcardActivity extends AppCompatActivity {
    ImageView imgarrowback;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editcard);
        imgarrowback=findViewById(R.id.arrrowcardedit);
        imgarrowback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(EditcardActivity.this,CardSettingActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}