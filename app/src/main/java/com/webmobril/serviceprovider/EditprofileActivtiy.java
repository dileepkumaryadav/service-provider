package com.webmobril.serviceprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class EditprofileActivtiy extends AppCompatActivity {

    RadioGroup radioGroup;
    RadioButton btnmale,btnfemale;
    String selectedRadioButton;
    ImageView imgarrow;
    Button btnsavedetail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editprofile);
        radioGroup=(RadioGroup)findViewById(R.id.btnradiogroup);
        btnmale=(RadioButton)findViewById(R.id.btnmale);
        btnfemale=(RadioButton)findViewById(R.id.btnfemale);
        btnsavedetail=findViewById(R.id.btnsavedetail);
        imgarrow=findViewById(R.id.imgarrrowEdit);
        imgarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(EditprofileActivtiy.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btnmale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnmale.isChecked()){
                    selectedRadioButton=btnmale.getText().toString();
                }else if (btnfemale.isChecked()){
                    selectedRadioButton=btnfemale.getText().toString();
                }else
                Toast.makeText(getApplicationContext(),selectedRadioButton,Toast.LENGTH_LONG).show();
            }
        });

    }
}