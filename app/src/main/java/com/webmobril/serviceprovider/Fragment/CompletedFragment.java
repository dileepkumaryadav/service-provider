package com.webmobril.serviceprovider.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.serviceprovider.R;
import com.webmobril.serviceprovider.adapter.CompleteAdapter;
import com.webmobril.serviceprovider.adapter.UpcomingAdapter;

public class CompletedFragment extends Fragment {
    RecyclerView recyclerView;
    public CompletedFragment() {
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.completfragment, container, false);
        recyclerView=view.findViewById(R.id.recyclerviewcomplete);
        setRecyclerView();
        return view;
    }
    private void setRecyclerView() {
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        CompleteAdapter completeAdapter=new CompleteAdapter(getActivity());
        recyclerView.setAdapter(completeAdapter);
    }
}

