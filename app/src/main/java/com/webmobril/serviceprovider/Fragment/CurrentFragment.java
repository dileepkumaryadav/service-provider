package com.webmobril.serviceprovider.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.serviceprovider.R;
import com.webmobril.serviceprovider.adapter.CurrentAdapter;

public class CurrentFragment extends Fragment {
    RecyclerView recyclerView;

    public CurrentFragment() {
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.currentfragment, container, false);
        recyclerView=view.findViewById(R.id.recyclerviewcurrent);
        setRecyclerView();
        return view;
    }
    private void setRecyclerView() {
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        CurrentAdapter currentAdapter=new CurrentAdapter(getActivity());
        recyclerView.setAdapter(currentAdapter);
    }
}
