package com.webmobril.serviceprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;

public class FreeTrailActivtiy extends AppCompatActivity {
    ImageView imgback;
    Button btnbuynow;
    Button btnbuynow1;

    RadioButton btnmonthly,btnquarterly,btnyearly;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_free_trail);
        imgback=findViewById(R.id.imgarrrowback);
        btnbuynow=findViewById(R.id.btnbuynow);
        btnbuynow1=findViewById(R.id.btnbuynow1);
        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(FreeTrailActivtiy.this,SignupActivtiy.class);
                startActivity(intent);
                finish();
            }
        });

        btnbuynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        btnbuynow1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(FreeTrailActivtiy.this,PaymentActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btnmonthly=(RadioButton)findViewById(R.id.btnmonthly);
        btnquarterly=(RadioButton)findViewById(R.id.btnquarterly);
        btnyearly=(RadioButton) findViewById(R.id.btnyearly);
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.btnmonthly:
                btnyearly.setChecked(!checked);
                btnquarterly.setChecked(!checked);
                btnbuynow.setVisibility(View.GONE);
                btnbuynow1.setVisibility(View.VISIBLE);
                break;
            case R.id.btnquarterly:
                btnmonthly.setChecked(!checked);
                btnyearly.setChecked(!checked);
                btnbuynow.setVisibility(View.GONE);
                btnbuynow1.setVisibility(View.VISIBLE);
                break;
            case R.id.btnyearly:
                btnmonthly.setChecked(!checked);
                btnquarterly.setChecked(!checked);
                btnbuynow.setVisibility(View.GONE);
                btnbuynow1.setVisibility(View.VISIBLE);
                break;
        }

    }
}