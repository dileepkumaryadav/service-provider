package com.webmobril.serviceprovider;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.renderscript.ScriptGroup;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.webmobril.serviceprovider.Fragment.CompletedFragment;
import com.webmobril.serviceprovider.Fragment.CurrentFragment;
import com.webmobril.serviceprovider.Fragment.UpcomingFreagment;
import com.webmobril.serviceprovider.databinding.ActivityMainBinding;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    ActivityMainBinding binding;
    private FragmentTransaction ft;
    private Fragment currentFragment;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private ImageView imgnotification;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(binding.layoutContent.toolbar.toolbarmain);
        init();
       // loadHome("0");


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        imgnotification = findViewById(R.id.imgnotification);
        addTabs(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        init();
        loadHome("0");
        imgnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, NotificationActivity.class);
                startActivity(intent);
            }
        });
        binding.layoutDrawer.topprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, EditprofileActivtiy.class);
                startActivity(intent);
            }
        });
        binding.layoutDrawer.tetSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,SubscriptionActivtiy.class);
                startActivity(intent);
            }
        });
        binding.layoutDrawer.tetchangepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,ChangepasswordActivity.class);
                startActivity(intent);
            }
        });
        binding.layoutDrawer.tetcardsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,CardSettingActivity.class);
                startActivity(intent);
            }
        });
        binding.layoutDrawer.tetcontact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,ContactusActivity.class);
                startActivity(intent);
            }
        });
        binding.layoutDrawer.tetratereview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,RatereviewActivity.class);
                startActivity(intent);
            }
        });
        binding.layoutDrawer.tetterm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,TermconditionActivity.class);
                startActivity(intent);
            }
        });
        binding.layoutDrawer.tetprivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,PrivacypolicyActivity.class);
                startActivity(intent);
            }
        });
        binding.layoutDrawer.tetservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,ServicemanagerActivity.class);
                startActivity(intent);
            }
        });
        binding.layoutDrawer.tetdocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,DocumentActivity.class);
                startActivity(intent);
            }
        });
        binding.layoutDrawer.tetaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,AddressActivity.class);
                startActivity(intent);
            }
        });
        binding.layoutDrawer.txtlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity.this,LoginActivtiy.class);
                startActivity(i);
            }
        });
        binding.layoutDrawer.tetcalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity.this,OfficetimeActivity.class);
                startActivity(i);
            }
        });
    }

    private void setSupportActionBar(ScriptGroup.Binding toolbar) {

    }

    private void addTabs(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new UpcomingFreagment(),"Upcoming Appointments");
        adapter.addFrag(new CurrentFragment(), "Current Appointments");
        adapter.addFrag(new CompletedFragment(), "Completed Appointments");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }
        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    private void init()
    {
        binding.layoutContent.toolbar.btnmenu.setOnClickListener(this);
        binding.layoutContent.toolbar.btnmenu.setOnClickListener(this);
        ;
       /*// binding.layoutContent.toolbar.imgbackOther
        binding.drawerMenuItems.linearLanguage.setOnClickListener(this);
        binding.drawerMenuItems.linearSubcription.setOnClickListener(this);
        binding.drawerMenuItems.linearLogout.setOnClickListener(this);*/
}
    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.btnmenu)
        {
            if (!binding.drawerLayout.isDrawerOpen(GravityCompat.START))
                binding.drawerLayout.openDrawer(GravityCompat.START);
            else binding.drawerLayout.closeDrawer(GravityCompat.START);
        }
        if (v.getId() == R.id.imgBack)
        {
            onBackPressed();
        }
    }

    private void loadFragmentOther(Fragment fragment, String type)
    {
        Bundle bundle = new Bundle();
      /*  bundle.putString(Constant.FRAGMENT_TYPE, type);*/
        ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.right_in, R.anim.left_out, R.anim.left_in, R.anim.right_out);
        ft.replace(R.id.frameMain, fragment);
        fragment.setArguments(bundle);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void toolbarOther(String title)
    {
       /* binding.layoutContent.toolbar.toolbarmain.setVisibility(View.GONE);
        binding.layoutContent.toolbar.toolbarmain.setVisibility(View.VISIBLE);*/
    }

    public void toolbarHome()
    {
      /* binding.layoutContent.toolbar.toolbarmain.setVisibility(View.VISIBLE);
     binding.layoutContent.toolbar.toolbarmain.setVisibility(View.GONE);*/
    }
    private void loadHome(String anim)
    {
        ft = getSupportFragmentManager().beginTransaction();
        currentFragment = new Fragment();
        if (anim.equals("1"))
        {
            ft.setCustomAnimations(R.anim.left_in, R.anim.right_out);
        }
        ft.replace(R.id.frameMain, currentFragment);
       // ft.addToBackStack(null);
        ft.commit();
    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else if (!doubleBackToExitPressedOnce) {
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this,"Please click BACK again to exit.", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            super.onBackPressed();
            return;
        }
    }
}
