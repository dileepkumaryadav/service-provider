package com.webmobril.serviceprovider;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.android.material.tabs.TabLayout;
import com.webmobril.serviceprovider.Fragment.OfficeFragment;
import com.webmobril.serviceprovider.Fragment.OnlineFragment;
import com.webmobril.serviceprovider.adapter.OfficetimeAdapter;

import java.util.ArrayList;
import java.util.List;

public class OfficetimeActivity extends AppCompatActivity {

    FrameLayout simpleFrameLayout;
    TabLayout tabLayout;
    ImageView img;
    private ViewPager viewPager;
    private RecyclerView recyclerView;
    private List<ListModel>listModels;
    private List<OnlineTime> Onlinetime;
    private Object HorizontalScrollView;
    private Object String;
    LinearLayoutManager HorizontalLayout;
    private Object OfficeTime;
    private Object OnlineTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_officetime);
        img = findViewById(R.id.imgarrrowOffice);
        viewPager = (ViewPager) findViewById(R.id.viewpageroffice);
        simpleFrameLayout = (FrameLayout) findViewById(R.id.simpleFrameLayout);
        tabLayout = (TabLayout) findViewById(R.id.simpleTabLayout);
        tabLayout.setupWithViewPager(viewPager);
        TabLayout.Tab firstTab = tabLayout.newTab();
        firstTab.setText("Office Timing");
        tabLayout.addTab(firstTab);
        TabLayout.Tab secondTab = tabLayout.newTab();
        secondTab.setText("Online Timing");
        tabLayout.addTab(secondTab);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Fragment fragment = null;
                switch (tab.getPosition()) {
                    case 0:
                        fragment = new OfficeFragment();
                        break;
                    case 1:
                        fragment = new OnlineFragment();
                        break;
                }
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.simpleFrameLayout, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OfficetimeActivity.this, MainActivity.class);
                startActivity(i);
            }
        });
        listModels = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        listModels.clear();
        listModels.add(new ListModel("Today, 16 sep", "19 slots available"));
        listModels.add(new ListModel("Tomorrow, 17 sep", "0 slots available"));
        listModels.add(new ListModel("Monday, 18 sep", "17 slots available"));
        listModels.add(new ListModel("Tuesday, 19 sep", "16 slots available"));
        listModels.add(new ListModel("Wednesday, 20 sep", "6 slots available"));
        listModels.add(new ListModel("Thursday, 21 sep", "6 slots available"));
        listModels.add(new ListModel("Friday, 22 sep", "6 slots available"));
        if (listModels.size() > 0) {
            recyclerView.setAdapter(new OfficetimeAdapter((Context) this, listModels));
        }
    }
}
