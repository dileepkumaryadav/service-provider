package com.webmobril.serviceprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class RatereviewActivity extends AppCompatActivity {
    ImageView imgratereview;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ratereview);
        imgratereview=findViewById(R.id.arrrowratereview);
        imgratereview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(RatereviewActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}