package com.webmobril.serviceprovider;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

public class SearchserviceActivity<recyclerView1> extends AppCompatActivity {
    Button btnaddervice;
    ImageView imgarrowback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchservice);
        btnaddervice = findViewById(R.id.btnaddservice);
        imgarrowback = findViewById(R.id.arrrowsearchservice);
        btnaddervice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchserviceActivity.this, AddserviceActivity.class);
                startActivity(intent);
            }
        });
        imgarrowback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchserviceActivity.this, ServicemanagerActivity.class);
                startActivity(intent);
            }
        });
    }
}