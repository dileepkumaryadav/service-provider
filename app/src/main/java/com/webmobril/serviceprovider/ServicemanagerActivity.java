package com.webmobril.serviceprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

public class ServicemanagerActivity extends AppCompatActivity {
    ImageView imgarrowback;
    EditText etservice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicemanager);
        etservice=findViewById(R.id.etservice);
        imgarrowback=findViewById(R.id.arrrowservicemanagement);
        imgarrowback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ServicemanagerActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
        etservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ServicemanagerActivity.this,SearchserviceActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}