package com.webmobril.serviceprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class SignupActivtiy extends AppCompatActivity {
    ImageView imageView;
    Button btnsignup;
    TextView tetLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        imageView=findViewById(R.id.arrowback);
        tetLogin=findViewById(R.id.tetLogin);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SignupActivtiy.this,LoginActivtiy.class);
                startActivity(intent);
            }
        });
        btnsignup=findViewById(R.id.btnsignup);
        btnsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SignupActivtiy.this,FreeTrailActivtiy.class);
                startActivity(intent);
            }
        });
        tetLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SignupActivtiy.this,LoginActivtiy.class);
                startActivity(i);
            }
        });
    }
}