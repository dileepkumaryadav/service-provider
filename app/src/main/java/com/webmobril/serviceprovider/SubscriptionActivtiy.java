package com.webmobril.serviceprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageView;

public class SubscriptionActivtiy extends AppCompatActivity {
    ImageView imgarrowback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_activtiy);
        imgarrowback=findViewById(R.id.imgarrrowsubscription);
        imgarrowback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SubscriptionActivtiy.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}