package com.webmobril.serviceprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class Upcoming1Activity extends AppCompatActivity {
    TextView txtchat;
    ImageView img,imgupcoming;
    Button btn;
    TextView txt;
    TextView  tetcancel;
    private ActionBar dialog;
    LinearLayout lLayoutAppoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upcoming1);
        txtchat = findViewById(R.id.txtchat);
        img = findViewById(R.id.imgchat);
        imgupcoming=findViewById(R.id.arrrowupcoming1);
        btn=findViewById(R.id.btn);
        txt=findViewById(R.id.txt);
        lLayoutAppoint=findViewById(R.id.lLayoutAppoint);
        tetcancel = findViewById(R.id.tetcancel);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Upcoming1Activity.this, ChatActivity.class);
                startActivity(i);
            }
        });
        txtchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Upcoming1Activity.this, ChatActivity.class);
                startActivity(i);
            }
        });
        imgupcoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Upcoming1Activity.this,MainActivity.class);
                startActivity(i);
            }
        });
        tetcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
    }
    public void showDialog(){
        final Dialog dialog = new Dialog(Upcoming1Activity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.alertdialog);
        TextView textyes = dialog.findViewById(R.id.tetYes);
        TextView textNo = dialog.findViewById(R.id.tetNo);
        textyes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lLayoutAppoint.setVisibility(View.VISIBLE);
                tetcancel.setVisibility(View.GONE);
                btn.setVisibility(View.GONE);
                txt.setVisibility(View.GONE);
                txtchat.setText("Cancel Chat");
                txtchat.setTextColor(getResources().getColor(R.color.red));
                dialog.dismiss();
                //Toast.makeText(getApplicationContext(),"Yes",Toast.LENGTH_SHORT).show();
              //  showDialog();
            }
        });
        textNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(),"No",Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });
        dialog.show();
    }
}
