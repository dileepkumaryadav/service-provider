package com.webmobril.serviceprovider.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.serviceprovider.R;

public class CompleteAdapter extends RecyclerView.Adapter<CompleteAdapter.CompleteViewHolder> {
    public CompleteAdapter(FragmentActivity activity) {

    }

    @NonNull
    @Override
    public CompleteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.itemcomplet, parent, false);
        CompleteAdapter.CompleteViewHolder viewHolder = new CompleteAdapter.CompleteViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CompleteViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class CompleteViewHolder extends RecyclerView.ViewHolder {
        public CompleteViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
