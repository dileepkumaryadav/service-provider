package com.webmobril.serviceprovider.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.serviceprovider.R;

public class CurrentAdapter extends RecyclerView.Adapter<CurrentAdapter.CurrentViewHolder>{
    Context context;
    public CurrentAdapter(Context context) {
        this.context=context;
    }

    @NonNull
    @Override
    public CurrentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.itemcurrent, parent, false);
        CurrentAdapter.CurrentViewHolder viewHolder = new CurrentAdapter.CurrentViewHolder(listItem);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull CurrentViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 1;
    }
    public class CurrentViewHolder extends RecyclerView.ViewHolder {
        public CurrentViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
