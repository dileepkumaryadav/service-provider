package com.webmobril.serviceprovider.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.serviceprovider.R;

public class OfficeAdapter extends RecyclerView.Adapter<OfficeAdapter.OfficeViewHolder> {
    Context context;
    public OfficeAdapter(Context context) {
        this.context = context;
    }
    @NonNull
    @Override
    public OfficeAdapter.OfficeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.office, parent, false);
        OfficeAdapter.OfficeViewHolder viewHolder = new OfficeAdapter.OfficeViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull OfficeAdapter.OfficeViewHolder holder, int position) {

    }
    @Override
    public int getItemCount() {
        return 1;
    }
    public class OfficeViewAdopter {
    }
    public class OfficeViewHolder extends RecyclerView.ViewHolder {
        public OfficeViewHolder(View listItem) {
            super(listItem);
        }
    }
}
