package com.webmobril.serviceprovider.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.serviceprovider.ListModel;
import com.webmobril.serviceprovider.R;

import java.text.BreakIterator;
import java.util.List;

public class OfficetimeAdapter extends RecyclerView.Adapter<OfficetimeAdapter.ViewHolder> {
    private Context context;
    private List<ListModel> listModels;
    private ViewHolder holder;
    private int position;

    public OfficetimeAdapter(Context context, List<ListModel> listModels)
    {
        this.context = context;
        this.listModels = listModels;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.officetime,parent,false);
        return new ViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        this.holder = holder;
        this.position = position;
        holder.name.setText(listModels.get(position).getName());
        holder.slots.setText(listModels.get(position).getTxt());
    }
    @Override
    public int getItemCount() {
        return listModels.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {

        public BreakIterator text;
        TextView slots,name;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.txtname);
            slots=itemView.findViewById(R.id.txtslots);
        }
    }
    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
    public class OfficetimeViewHolder extends RecyclerView.ViewHolder {
        public OfficetimeViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}







