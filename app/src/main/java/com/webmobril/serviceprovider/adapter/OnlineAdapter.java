package com.webmobril.serviceprovider.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.serviceprovider.R;

public class OnlineAdapter extends RecyclerView.Adapter<OnlineAdapter.OnlineViewHolder> {
    Context context;

    public OnlineAdapter(Context context) {
        this.context = context;
    }
    @NonNull
    @Override
    public OnlineAdapter.OnlineViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.online, parent, false);
        OnlineAdapter.OnlineViewHolder viewHolder = new OnlineAdapter.OnlineViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull OnlineAdapter.OnlineViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 1;
    }
    public class OnlineViewHolder extends RecyclerView.ViewHolder {
        public OnlineViewHolder(View listItem) {
            super(listItem);
        }
    }
}

