package com.webmobril.serviceprovider.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.serviceprovider.OfficetimeActivity;
import com.webmobril.serviceprovider.OnlineTime;
import com.webmobril.serviceprovider.R;

import java.util.List;

public class OnlinetimeAdapter extends RecyclerView.Adapter<OnlinetimeAdapter.ViewHolder> {
    private Context context;
    private List<OnlineTime> onlineTimes;
    public OnlinetimeAdapter(Context context, List<OnlineTime> OnlineTime)
    {
        this.context = context;
        this.onlineTimes = onlineTimes;
    }
    public OnlinetimeAdapter(OfficetimeActivity context, Object OnlineTime) {
    }

    @NonNull
    @Override
    public OnlinetimeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.onlinetime,parent,false);
        return new OnlinetimeAdapter.ViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtname.setText( onlineTimes.get(position).getName());
        holder.txt.setText( onlineTimes.get(position).getTxt());
    }
    @Override
    public int getItemCount() {
        return  onlineTimes.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtname,txt;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtname= itemView.findViewById(R.id.txtname);
            txt=itemView.findViewById(R.id.txt);

        }
    }
    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }
    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
    public class OnlinetimeViewHolder {
    }
}
