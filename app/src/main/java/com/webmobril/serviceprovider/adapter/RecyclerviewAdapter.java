package com.webmobril.serviceprovider.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.serviceprovider.R;
import com.webmobril.serviceprovider.Upcoming;

import java.util.List;

public class RecyclerviewAdapter extends RecyclerView.Adapter<RecyclerviewAdapter.ViewHoldar> {
    Context mcontext;
    List<Upcoming>mData;

    public RecyclerviewAdapter(Context mcontext, List<Upcoming> mData) {
        this.mcontext = mcontext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public ViewHoldar onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;
        v= LayoutInflater.from(mcontext).inflate(R.layout.itemupcoming,parent,false);
        ViewHoldar vHoldar=new ViewHoldar(v);


        return vHoldar;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoldar holder, int position) {
        holder.tetname.setText(mData.get(position).getName());
        holder.img.setImageResource(mData.get(position).getPhoto());

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
    private  TextView tetname;
    private ImageView img;



    public static class ViewHoldar extends RecyclerView.ViewHolder{
        private final TextView tetname;
        private final ImageView img;

        public ViewHoldar(@NonNull View itemView) {
            super(itemView);

            tetname=(TextView)itemView.findViewById(R.id.tetnamejohn);
            img=(ImageView)itemView.findViewById(R.id.imageprofile);

        }
    }

}
