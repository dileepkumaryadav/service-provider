package com.webmobril.serviceprovider.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.webmobril.serviceprovider.R;
import com.webmobril.serviceprovider.Upcoming;
import com.webmobril.serviceprovider.Upcoming1Activity;

import static android.media.CamcorderProfile.get;

public class UpcomingAdapter extends RecyclerView.Adapter<UpcomingAdapter.UpcomingViewHolder> {
    Context context;
    public UpcomingAdapter(Context context) {
        this.context=context;
    }
    @NonNull
    @Override
    public UpcomingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.itemupcoming, parent, false);
        UpcomingViewHolder viewHolder = new UpcomingViewHolder(listItem);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(@NonNull UpcomingViewHolder holder, int position) {

        }
    @Override
    public int getItemCount() {
        return 1;
    }
    public class UpcomingViewHolder extends RecyclerView.ViewHolder {
        public UpcomingViewHolder(@NonNull final View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i=new Intent(v.getContext(), Upcoming1Activity.class);
                    v.getContext().startActivity(i);
                }
            });
        }
    }
}
